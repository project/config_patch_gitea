# Config Patch Gitea

This module allows to create a Pull Request on gitea from changed configuration.

It uses the Gitea module to make a connection to gitea.

## Install

composer require drupal/config_patch_gitea

Install as usual


Provide a link to the gitea server, add a repository and an api key at 
/admin/config/development/gitea