<?php


namespace Drupal\config_patch_gitea\Plugin\config_patch\output;

use Drupal;
use Drupal\config_patch\Plugin\config_patch\output\CliOutputPluginInterface;
use Drupal\config_patch\Plugin\config_patch\output\OutputPluginBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\StorageInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Serialization\Yaml;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\State\StateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Output patches as Pull request in Gitea.
 *
 * @ConfigPatchOutput(
 *  id = "config_patch_gitea",
 *  label = @Translation("Gitea"),
 *  action = @Translation("Create pull request")
 * )
 */
class Gitea extends OutputPluginBase implements ContainerFactoryPluginInterface, CliOutputPluginInterface {

  use StringTranslationTrait;
  use MessengerTrait;

  /**
   * @var \Drupal\Core\Config\StorageInterface
   */
  protected $configStorageExport;

  /**
   * @var \Drupal\Core\Config\StorageInterface
   */
  protected $configStorageSync;

  /**
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * @var
   */
  protected $clientFactory;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition,
                              StorageInterface $config_storage_export,
                              StorageInterface $config_storage_sync,
                              StateInterface $state,
                              ConfigFactoryInterface $config_factory, $clientFactory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->configStorageExport = $config_storage_export;
    $this->configStorageSync = $config_storage_sync;
    $this->config = $config_factory->get('config_patch.settings');
    $this->state = $state;
    $this->clientFactory = $clientFactory;
  }

  /**
   * @inheritDoc
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.storage.export'),
      $container->get('config.storage.sync'),
      $container->get('state'),
      $container->get('config.factory'),
      $container->get('gitea.client')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function output(array $patches, FormStateInterface $form_state) {
    $changes = $form_state->get('collections');
    $changes = reset($changes);
    $selected_collections = $form_state->getValue('list');
    $merge_from = $form_state->getValue('merge_from');
    $merge_into = $form_state->getValue('merge_into');
    $new_branch = $form_state->getValue('new_branch');
    $commit_message = $form_state->getValue('commit_message');
    $new_branch = 'config-patch-' . time();

    $actions = $this->collectActions($changes, $patches, $selected_collections);

    // @todo do this in a queue
    $this->performActions($actions, $commit_message, $merge_into, $merge_from, $new_branch);
  }

  /**
   * {@inheritdoc}
   */
  public function outputCli(array $patches, array $config_changes = [], array $params = []) {
    // @todo should this work from cli?
    //$actions = $this->collectGitlabActions($config_changes);
    //return $this->performGitlabActions($actions, $params['message']);
  }


  /**
   * Collects actions for commit api request.
   *
   * @param array $changes
   *   Configuration changes grouped by collection name.
   * @param array $patches
   *   List of patches to export.
   * @param array $selected_collections
   *   The collections that were selected.
   *
   *
   * @return array
   *   Actions for the commit.
   */
  private function collectActions(array $changes, array $patches, array $selected_collections) {
    $actions = [];
    foreach ($changes as $collection_name => $collection) {
      if ($selected_collections[$collection_name]) {
        $action_key = $collection['type'];
        if (isset($patches['default'][$collection_name])) {
          $actions[$action_key][$collection_name] = $patches['default'][$collection_name];
        }
      }
    }
    return $actions;
  }

  /**
   * Commit the changes and create merge request.
   *
   * @param array $actions
   * @param string $commit_message
   * @param string $merge_from
   * @param string $merge_into
   * @param string $new_branch
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   */
  private function performActions(array $actions, string $commit_message,
                                  string $merge_into, string $merge_from,
                                  string $new_branch) {

    $error = FALSE;

    // create a new branch
    if ($this->clientFactory->createBranch($new_branch, $merge_from)) {

      // commit all changes
      foreach ($actions as $action => $files) {
        foreach ($files as $key => $patch) {
          $file_path = 'config/default/' . $key . '.yml';
          $content = $this->getTargetFileContent($key);
          if ($action == 'create') {
            if (!$this->clientFactory->createFile($file_path, $content, $new_branch, $commit_message)) {
              $error = TRUE;
              break;
            }
          }
          if ($action == 'update') {
            $repo_file = $this->clientFactory->getFile($file_path, $merge_from);
            if (!$this->clientFactory->updateFile($file_path, $content, $repo_file->sha, $new_branch, $commit_message)) {
              $error = TRUE;
              break;
            }
          }
          if ($action == 'delete') {
            $repo_file = $this->clientFactory->getFile($file_path, $merge_from);
            if (!$this->clientFactory->deleteFile($file_path, $repo_file->sha, $new_branch, $commit_message)) {
              $error = TRUE;
              break;
            }
          }
          if ($action == 'rename') {
            // @todo rename files (is  this possible from config?)
          }
        }
      }
      if ($error) {
        \Drupal::messenger()->addError('There was a problem adding the files to git.');
        $this->clientFactory->deleteBranch($new_branch);
      }
      else {
        // allright, great success! lets create a PR
        $pr = $this->clientFactory->createPullRequest($merge_into, $new_branch, $commit_message);
        $link = Link::fromTextAndUrl(t('Check it out'), Url::fromUri($pr->url));
        \Drupal::messenger()->addMessage(t('A new PR was created. @link.', ['@link' => $link->toString()]));
      }
    }
    else {
      \Drupal::messenger()->addError('There was a problem creating a new branch');
    }


  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return array
   */
  public function alterForm(array $form, FormStateInterface $form_state) {
    $form = parent::alterForm($form, $form_state);
    /*$form['new_branch'] = [
      '#type' => 'textfield',
      '#title' => $this->t('New branch name'),
      '#weight' => -110,
      '#required' => TRUE,
    ];*/
    $form['merge_from'] = [
      '#type' => 'select',
      '#title' => $this->t('Merge from'),
      '#weight' => -109,
      '#options' => $this->clientFactory->getBranchesAsOptions(),
      '#default_value' => $this->clientFactory->getDefaultBranch(),
      '#required' => TRUE,
    ];
    $form['merge_into'] = [
      '#type' => 'select',
      '#title' => $this->t('Merge into'),
      '#weight' => -109,
      '#options' => $this->clientFactory->getBranchesAsOptions(),
      '#default_value' => $this->clientFactory->getDefaultBranch(),
      '#required' => TRUE,
    ];
    $form['commit_message'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Commit message'),
      '#weight' => -100,
      '#required' => TRUE,
    ];
    return $form;
  }

  /**
   * Get the contents of the target file
   *
   * @param [type] $target_name
   * @param [type] $collection
   * @return string contents of the targer file
   */
  protected function getTargetFileContent($target_name = NULL, $collection = StorageInterface::DEFAULT_COLLECTION) {
    if ($collection != StorageInterface::DEFAULT_COLLECTION) {
      $target_storage = $this->configStorageExport->createCollection($collection);
    } else {
      $target_storage = $this->configStorageExport;
    }

    $raw_target = $target_storage->read($target_name);
    $source_data = $raw_target ? Yaml::encode($raw_target) : NULL;

    return $source_data;
  }

  /**
   * Get the contents of the source file
   *
   * @param [type] $target_name
   * @param [type] $collection
   * @return string contents of the targer file
   */
  protected function getSourceFileContent($target_name = NULL, $collection = StorageInterface::DEFAULT_COLLECTION) {
    if ($collection != StorageInterface::DEFAULT_COLLECTION) {
      $target_storage = $this->configStorageSync->createCollection($collection);
    } else {
      $target_storage = $this->configStorageSync;
    }

    $raw_target = $target_storage->read($target_name);
    $source_data = $raw_target ? Yaml::encode($raw_target) : NULL;

    return $source_data;
  }

}
